#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <qurt.h>

const char SLPI_MODULE_NAME[] = "voxl-slpi-uart-bridge-slpi";

// TODO: This has to be defined in the slpi_proc build and in this build.
// Make it accessible from one file to both builds.
typedef struct {
    int (*advertise_func_ptr)(const char *topic_name);
    int (*subscribe_func_ptr)(const char *topic_name);
    int (*unsubscribe_func_ptr)(const char *topic_name);
    int (*topic_data_func_ptr)(const char *name, const uint8_t *data, int data_len_in_bytes);
    int (*config_spi_bus)();
    int (*spi_transfer)(int fd, const uint8_t *send, uint8_t *recv, const unsigned len);
    int (*config_i2c_bus)(uint8_t bus_number, uint8_t address, uint32_t frequency);
    void (*set_i2c_address)(int fd, uint8_t address);
    int (*i2c_transfer)(int fd, const uint8_t *send, const unsigned send_len, uint8_t *recv, const unsigned recv_len);
    int (*config_uart)(uint8_t port_number, uint32_t speed);
    int (*uart_write)(int fd, const char *data, const unsigned data_len);
    int (*uart_read)(int fd, char *buffer, const unsigned buffer_len);
    int (*register_interrupt_callback)(int (*)(int, void*, void*), void* arg);
} fc_func_ptrs;


extern void HAP_debug(const char *msg, int level, const char *filename, int line);

#define PRINT_BUFFER_SIZE 256
char print_buffer[PRINT_BUFFER_SIZE];

void debug_print(int level, const char* fmt, ...)
{
  memset(&print_buffer,0, PRINT_BUFFER_SIZE);

  va_list args;
  va_start(args,fmt);
  int len = vsnprintf(print_buffer, PRINT_BUFFER_SIZE, fmt, args);
  va_end(args);

  HAP_debug(print_buffer, level, __FILE__, __LINE__);
}

#define USE_TX_THREAD 0
#define USE_RX_THREAD 1

#define UART_THREAD_STACK_SIZE 8192
static char rx_thread_stack[UART_THREAD_STACK_SIZE];
static char tx_thread_stack[UART_THREAD_STACK_SIZE];
static char idle_thread_stack[UART_THREAD_STACK_SIZE];
static qurt_sem_t tx_sem;

static fc_func_ptrs _func_ptrs;

static int uart_fd = -1;

static bool uart_rx_thread_running = false;
static bool uart_tx_thread_running = false;
static bool idle_thread_running    = false;

#define UART_BUFFER_MAX_SIZE 1024

#define UART_TX_BUFFER_COUNT 16
static char uart_tx_buffer[UART_TX_BUFFER_COUNT][UART_BUFFER_MAX_SIZE];
static int  uart_tx_buffer_len[UART_TX_BUFFER_COUNT];
static int tx_write_index = 0;
static int tx_read_index = 0;
static qurt_mutex_t tx_mutex;
static qurt_sem_t tx_sem;

static char uart_rx_buffer[UART_BUFFER_MAX_SIZE];

static void idle_thread(void * test){
  while(1)
  {
    qurt_timer_sleep(100);
  }

  qurt_thread_exit(0);
}

static void uart_tx_thread(void *test) {
    HAP_debug("uart_tx_thread starting", 1, "init", 0);

    while (true) {
        qurt_sem_down(&tx_sem);

        // qurt_mutex_lock(&tx_mutex);
        if ((uart_fd != -1) && (_func_ptrs.uart_write)) {
            // Copy the data into our local storage
            int bytes_written = _func_ptrs.uart_write(uart_fd, uart_tx_buffer[tx_read_index], uart_tx_buffer_len[tx_read_index]);
            if (bytes_written != uart_tx_buffer_len[tx_read_index]) {
                HAP_debug("tx failure", 1, "init", 0);
            }
        }
        tx_read_index++;
        if (tx_read_index == UART_TX_BUFFER_COUNT) tx_read_index = 0;
        // qurt_mutex_unlock(&tx_mutex);
    }

    uart_tx_thread_running = false;

    HAP_debug("uart_tx_thread exiting", 1, "init", 0);

	qurt_thread_exit(0);
}

static void uart_rx_thread(void *test) {
    HAP_debug("uart_rx_thread starting", 1, "init", 0);

    while (true) {
        if ((uart_fd != -1) && (_func_ptrs.uart_read) && (_func_ptrs.topic_data_func_ptr)) {
            int bytes_read = _func_ptrs.uart_read(uart_fd, uart_rx_buffer, UART_BUFFER_MAX_SIZE);
            if (bytes_read > 0) {
                //debug_print(1,"read %d bytes from uart",bytes_read);
                // HAP_debug("Read some data from UART", 1, "init", 0);
                _func_ptrs.topic_data_func_ptr("uart_data", (const uint8_t*) uart_rx_buffer, bytes_read);
            }
        }

        qurt_timer_sleep(100);
    }

    uart_rx_thread_running = false;

    HAP_debug("uart_rx_thread exiting", 1, "init", 0);

	qurt_thread_exit(0);
}


//ports 2, 6, 7 are supported right now
static int uart_open(int port_num, int baud_rate)
{

  //if ((uart_fd == -1) && (_func_ptrs.config_uart)) {
  if (_func_ptrs.config_uart) {
      debug_print(1, "Initializing uart %d @ %d", port_num, baud_rate);

      // Open UART port
      //uart_fd = _func_ptrs.config_uart(2, 921600);
      uart_fd = _func_ptrs.config_uart(port_num, baud_rate);
      if (uart_fd == -1) {
          debug_print(1,"UART open failed");
          return -1;
      } else {
          debug_print(1, "UART open succeeded");
      }
  }

  qurt_thread_t tid;
	qurt_thread_attr_t attr;
/*
  if ( !idle_thread_running) {
    qurt_thread_attr_init(&attr);
    qurt_thread_attr_set_stack_addr(&attr, idle_thread_stack);
    qurt_thread_attr_set_stack_size(&attr, UART_THREAD_STACK_SIZE);
    qurt_thread_attr_set_priority (&attr, 255);
    int status = qurt_thread_create(&tid, &attr, &idle_thread, NULL);

      if (status == QURT_EFAILED) {
          HAP_debug("Error starting IDLE thread", 1, "init", 0);
          return -1;
      } else {
          HAP_debug("Starting IDLE thread", 1, "init", 0);
          idle_thread_running = true;
      }
  }
*/
  if (USE_RX_THREAD) {
      if ( ! uart_rx_thread_running) {
      	qurt_thread_attr_init(&attr);
      	qurt_thread_attr_set_stack_addr(&attr, rx_thread_stack);
      	qurt_thread_attr_set_stack_size(&attr, UART_THREAD_STACK_SIZE);
      	int status = qurt_thread_create(&tid, &attr, &uart_rx_thread, NULL);

          if (status == QURT_EFAILED) {
              HAP_debug("Error starting UART receive thread", 1, "init", 0);
              return -1;
          } else {
              HAP_debug("Starting UART receive thread", 1, "init", 0);
              uart_rx_thread_running = true;
          }
      }
      else
      {
        HAP_debug("UART receive thread is already running", 1, "init", 0);
      }
  }

  if (USE_TX_THREAD) {
      if ( ! uart_tx_thread_running) {
          qurt_mutex_init(&tx_mutex);
          qurt_sem_init_val(&tx_sem, 0);

      	qurt_thread_attr_init(&attr);
      	qurt_thread_attr_set_stack_addr(&attr, tx_thread_stack);
      	qurt_thread_attr_set_stack_size(&attr, UART_THREAD_STACK_SIZE);
      	int status = qurt_thread_create(&tid, &attr, &uart_tx_thread, NULL);

          if (status == QURT_EFAILED) {
              HAP_debug("Error starting UART transmit thread", 1, "init", 0);
              return -1;
          } else {
              HAP_debug("Starting UART transmit thread", 1, "init", 0);
              uart_tx_thread_running = true;
          }
      }
      else
      {
        HAP_debug("UART transmit thread is already running", 1, "init", 0);
      }
  }

  return 0;
}

int px4muorb_orb_initialize(fc_func_ptrs *func_ptrs, int32_t clock_offset_us)
{
	// HAP_debug("px4muorb_orb_initialize called", 1, "init", 0);

  if (func_ptrs != NULL) {
   HAP_debug("func ptrs are not null", 1, "init", 0);
      _func_ptrs = *func_ptrs;
  }

	return 0;
}

int px4muorb_topic_advertised(const char *topic_name)
{
	return 0;
}

int px4muorb_add_subscriber(const char *topic_name)
{
	return 0;
}

int px4muorb_remove_subscriber(const char *topic_name)
{
	return 0;
}


typedef struct
{
  uint32_t port_number;
  uint32_t baud_rate;
} uart_config_t;

//this function is called with data from CPU
int px4muorb_send_topic_data(const char *topic_name, const uint8_t *data,
			     int data_len_in_bytes)
{
    if (strcmp("uart_config", topic_name) == 0) {
        uart_config_t c;

        if (data_len_in_bytes != sizeof(c))
        {
            debug_print(1,"invalid data length for uart_config: %d != %d", data_len_in_bytes,sizeof(c));
            return -1;
        }

        memcpy(&c,data, sizeof(c));

        return uart_open(c.port_number,c.baud_rate);
    }

    if (strcmp("uart_data", topic_name) == 0) {
        if (USE_TX_THREAD) {
            if ((data_len_in_bytes > 0) && (data_len_in_bytes < UART_BUFFER_MAX_SIZE)) {
                int temp_tx_read_index = 0;

                qurt_mutex_lock(&tx_mutex);
                // Copy the data into our local storage
                memcpy(uart_tx_buffer[tx_write_index], data, data_len_in_bytes);
                uart_tx_buffer_len[tx_write_index] = data_len_in_bytes;
                tx_write_index++;
                if (tx_write_index == UART_TX_BUFFER_COUNT) tx_write_index = 0;
                temp_tx_read_index = tx_read_index;
                qurt_mutex_unlock(&tx_mutex);

                qurt_sem_up(&tx_sem);

                if (tx_write_index == temp_tx_read_index) {
        	       HAP_debug("Error: write pointer caught up to read pointer", 1, "init", 0);
                }
            } else {
        	    HAP_debug("Error: got request to send incorrect number of bytes", 1, "init", 0);
            }
        }
        else {
            (void) _func_ptrs.uart_write(uart_fd, (const char*) data, data_len_in_bytes);
            debug_print(1,"wrtten %d bytes to uart",data_len_in_bytes);
        }
    }

	return 0;
}
