#!/bin/bash
set -e -x

echo "[INFO] Starting SLPI build"
cd slpi
rm -rf build
mkdir build
cd build
cmake ../
make
cd ../../
echo "[INFO] Finished SLPI build"

echo "[INFO] Starting APPS build"
cd apps
rm -rf build
mkdir build
cd build
cmake ../
make
cd ../../
echo "[INFO] Finished APPS build"