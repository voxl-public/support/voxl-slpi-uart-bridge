
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "voxl_slpi_uart_bridge_interface.h"
#include "fc_sensor.h"

static int initialized = 0;
bool enable_debug = false;

#define RX_BUF_SIZE 1024
uint8_t rx_buffer[RX_BUF_SIZE];
uint8_t return_buffer[RX_BUF_SIZE];
uint32_t rx_buf_head = 0;
uint32_t rx_buf_tail = 0;

pthread_mutex_t read_mx = PTHREAD_MUTEX_INITIALIZER;

static void receive_callback(const char *topic,
                      const uint8_t *data,
                      uint32_t length_in_bytes) {
    // printf("Got %u bytes of UART data: %s\n", length_in_bytes, data);
    //printf("%s", data);

    pthread_mutex_lock(&read_mx);
    for (int ii=0; ii<length_in_bytes; ii++)
    {
      rx_buffer[rx_buf_head] = data[ii];
      rx_buf_head++;
      if (rx_buf_head >= RX_BUF_SIZE)
        rx_buf_head = 0;
    }
    pthread_mutex_unlock(&read_mx);
}

static void unused_callback(const char *topic)
{
    printf("Got unused callback for topic %s\n", topic);
}

uint8_t* voxl_rpc_shared_mem_alloc(size_t bytes)
{
    return &(return_buffer[0]);
}


void voxl_rpc_shared_mem_free(uint8_t* ptr)
{
  return;
}


void voxl_rpc_shared_mem_deinit()
{
  return;
}

int voxl_uart_close(int bus)
{
  return 0;
}

int voxl_uart_flush(int bus)
{
  return 0;
}

int voxl_uart_drain(int bus)
{
  return 0;
}

typedef struct
{
  uint32_t port_number;
  uint32_t baud_rate;
} uart_config_t;

int voxl_uart_init(int bus, int baud_rate)
{
  if (initialized)
  {
    if (enable_debug)
        printf("voxl_uart_init already initialized\n");
  }
  else
  {
    fc_callbacks cb = {&receive_callback, &unused_callback,
                       &unused_callback, &unused_callback};

    if (fc_sensor_set_library_name("libslpi_uart_bridge_slpi.so") != 0) {
        fprintf(stderr, "Error setting library name in voxl_uart_init\n");
        return -1;
    }

    if (fc_sensor_initialize(enable_debug, &cb) != 0) {
        fprintf(stderr, "Error calling voxl_uart_init\n");
        return -1;
    } else if (enable_debug) {
        printf("voxl_uart_init succeeded\n");
    }
    initialized = 1;
  }

  uart_config_t c;
  c.port_number = bus;
  c.baud_rate   = baud_rate;

  int status = fc_sensor_send_data("uart_config", (const uint8_t*) &c, sizeof(c));

  if (status)
  {
    printf("uart_config failed\n");
    return -1;
  }

  return 0;
}


int voxl_uart_read(int bus, uint8_t* data, size_t dataLen)
{
  if (rx_buf_head == rx_buf_tail)
    return 0;

  uint32_t num_bytes_returned = 0;

  pthread_mutex_lock(&read_mx);
  for (int ii=0; ii<dataLen; ii++)
  {
    data[ii] = rx_buffer[rx_buf_tail];
    num_bytes_returned++;

    rx_buf_tail++;

    if (rx_buf_tail >= RX_BUF_SIZE)
      rx_buf_tail = 0;

    if (rx_buf_head == rx_buf_tail)
      break;
  }
  pthread_mutex_unlock(&read_mx);

  return num_bytes_returned;
}

int voxl_uart_write(int bus, uint8_t* data, size_t bytes)
{
  int status = fc_sensor_send_data("uart_data", (const uint8_t*) data, bytes);

  if (status) printf("fc_sensor_send_data failed\n");

  return status;
}
