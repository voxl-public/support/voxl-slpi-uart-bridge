# voxl-slpi-uart-bridge

Interface Library for VOXL2 that provides a bridge between SLPI UART ports and CPU

# Notes
- Only tested on one UART port at a time, probably does not work for multiple concurrent open ports
