import ctypes as ct
import time

libvoxl_io = ct.CDLL("libslpi_uart_bridge_cpu.so")

voxl_uart_init  = libvoxl_io.voxl_uart_init
voxl_uart_init.argtypes = [ct.c_int,ct.c_int]
voxl_uart_init.restype  = ct.c_int

# close an initialized serial port
voxl_uart_close = libvoxl_io.voxl_uart_close
voxl_uart_close.argtypes = [ct.c_int]
voxl_uart_close.restype  = ct.c_int

# write bytes to an initialized serial port
voxl_uart_write          = libvoxl_io.voxl_uart_write
voxl_uart_write.argtypes = [ct.c_int, ct.POINTER(ct.c_uint8), ct.c_size_t]
voxl_uart_write.restype  = ct.c_int

# read bytes from an initialized serial port
voxl_uart_read           = libvoxl_io.voxl_uart_read
voxl_uart_read.argtypes  = [ct.c_int, ct.POINTER(ct.c_uint8), ct.c_size_t]
voxl_uart_read.restype   = ct.c_int

class VoxlSerialPort():
    initialized   = False
    _baudrate      = 0
    port          = ''
    port_num      = -1
    parity        = 0
    bytesize      = 8
    stopbits      = 1
    timeout       = 0
    writeTimeout  = 0
    read_buf_ptr  = None
    read_buf_size = 256

    def __init__(self):
        self.initialized = False

    def isOpen(self):
        return self.initialized

    def inWaiting(self):
        return self.read_buf_size

    def open(self):
        # self.port_num = int(self.port.split('-')[-1])
        # self._baudrate = int(self._baudrate)
        voxl_uart_init(self.port_num, self._baudrate)
        # self.read_buf_ptr = voxl_rpc_shared_mem_alloc(self.read_buf_size)
        self.initialized  = True

    @property
    def baudrate(self):
        return self._baudrate

    @baudrate.setter
    def baudrate(self,value):
        self._baudrate = value
        if self.initialized:
            voxl_uart_init(self.port_num, self._baudrate)
            print('Updated baud rate to %d' % self._baudrate)

    def close(self):
        if not self.initialized:
            raise Exception('port is not initialized')

        voxl_uart_close(self.port_num)

    def read(self, num_bytes_to_read):
        if num_bytes_to_read > self.read_buf_size:
            num_bytes_to_read = self.read_buf_size
        bytes_read = voxl_uart_read(self.port_num,self.read_buf_ptr,num_bytes_to_read)
        data_array = bytearray(self.read_buf_ptr[0:bytes_read])
        return data_array

    def write(self, data):
        if not self.initialized:
            raise Exception('port is not initialized')
        # data = np.array(data)
        # data_ptr = data.ctypes.data_as(ct.POINTER(ct.c_uint8))
        # voxl_uart_write(self.port_num,data_ptr,data.nbytes)

ser = VoxlSerialPort()
ser.port_num = 2
ser._baudrate = 115200
ser.open()

